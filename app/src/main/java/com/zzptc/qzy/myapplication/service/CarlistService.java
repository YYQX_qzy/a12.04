package com.zzptc.qzy.myapplication.service;

import com.zzptc.qzy.myapplication.entity.CarGuzhangInfo;
import com.zzptc.qzy.myapplication.entity.CarInfo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CarlistService {
    @GET("carlist")
    Call<CarInfo> getAll();

    @GET("obdcode/query")
    Call<CarGuzhangInfo> getCarGuzhangAll(@Query("key")String key,@Query("code")String code);

}
