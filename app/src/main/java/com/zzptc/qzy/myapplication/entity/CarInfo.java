package com.zzptc.qzy.myapplication.entity;

import java.io.Serializable;
import java.util.List;

public class CarInfo implements Serializable {

    /**
     * RESULT : S
     * ERRMSG : 成功
     * data : [{"carplate":"宝马","carno":"辽A10001","who":"张三","amount":100,"$min":0,"$max":10000,"speedmin":0,"speedmax":1000,"speed":100,"stop":false},{"carplate":"中华","carno":"辽A10002","who":"李四","amount":99,"$min":0,"$max":10000,"speedmin":0,"speedmax":1000,"speed":100,"stop":false},{"carplate":"奔驰","carno":"辽A10003","who":"王五","amount":103,"$min":0,"$max":10000,"speedmin":0,"speedmax":1000,"speed":100,"stop":false},{"carplate":"马自达","carno":"辽A10004","who":"赵六","amount":1,"$min":0,"$max":10000,"speedmin":0,"speedmax":1000,"speed":100,"stop":false},{"carplate":"斯柯达","carno":"辽A10005","who":"上官六","amount":0,"$min":0,"$max":10000,"speedmin":0,"speedmax":1000,"speed":100,"stop":false}]
     */

    private String RESULT;
    private String ERRMSG;
    private List<DataDTO> data;

    public String getRESULT() {
        return RESULT;
    }

    public void setRESULT(String RESULT) {
        this.RESULT = RESULT;
    }

    public String getERRMSG() {
        return ERRMSG;
    }

    public void setERRMSG(String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }

    public List<DataDTO> getData() {
        return data;
    }

    public void setData(List<DataDTO> data) {
        this.data = data;
    }

    public static class DataDTO {
        /**
         * carplate : 宝马
         * carno : 辽A10001
         * who : 张三
         * amount : 100
         * $min : 0
         * $max : 10000
         * speedmin : 0
         * speedmax : 1000
         * speed : 100
         * stop : false
         */

        private String carplate;
        private String carno;
        private String who;
        private int amount;
        private int $min;
        private int $max;
        private int speedmin;
        private int speedmax;
        private int speed;
        private boolean stop;

        public String getCarplate() {
            return carplate;
        }

        public void setCarplate(String carplate) {
            this.carplate = carplate;
        }

        public String getCarno() {
            return carno;
        }

        public void setCarno(String carno) {
            this.carno = carno;
        }

        public String getWho() {
            return who;
        }

        public void setWho(String who) {
            this.who = who;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public int get$min() {
            return $min;
        }

        public void set$min(int $min) {
            this.$min = $min;
        }

        public int get$max() {
            return $max;
        }

        public void set$max(int $max) {
            this.$max = $max;
        }

        public int getSpeedmin() {
            return speedmin;
        }

        public void setSpeedmin(int speedmin) {
            this.speedmin = speedmin;
        }

        public int getSpeedmax() {
            return speedmax;
        }

        public void setSpeedmax(int speedmax) {
            this.speedmax = speedmax;
        }

        public int getSpeed() {
            return speed;
        }

        public void setSpeed(int speed) {
            this.speed = speed;
        }

        public boolean isStop() {
            return stop;
        }

        public void setStop(boolean stop) {
            this.stop = stop;
        }
    }
}
