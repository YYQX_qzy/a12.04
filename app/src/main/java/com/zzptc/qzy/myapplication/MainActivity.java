package com.zzptc.qzy.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.zzptc.qzy.myapplication.adapter.CarGuzhangAdapter;
import com.zzptc.qzy.myapplication.entity.CarGuzhangInfo;
import com.zzptc.qzy.myapplication.entity.CarInfo;
import com.zzptc.qzy.myapplication.service.CarlistService;

import org.litepal.LitePal;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    TextView textView;
    ListView listView;
    List<CarGuzhangInfo> carGuzhangInfos=new ArrayList<>();
    CarGuzhangAdapter adapter;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView=findViewById(R.id.tv);
        //getCarInfo();
        getCarGuzhangInfo();
        initView();
    }

    private void initView() {
        listView=findViewById(R.id.lv_main);
        carGuzhangInfos= LitePal.findAll(CarGuzhangInfo.class);
        adapter=new CarGuzhangAdapter(getApplicationContext(),R.layout.carguzhang_item,carGuzhangInfos);
        listView.setAdapter(adapter);
    }

    private void getCarGuzhangInfo() {
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl("http://apis.juhe.cn/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        CarlistService carlistService=retrofit.create(CarlistService.class);
        Call<CarGuzhangInfo> carGuzhangAll = carlistService
                .getCarGuzhangAll("6b40135084049a9641f5dc01af7ed188", "P2079");
        carGuzhangAll.enqueue(new Callback<CarGuzhangInfo>() {
            @Override
            public void onResponse(Call<CarGuzhangInfo> call, Response<CarGuzhangInfo> response) {
                CarGuzhangInfo qi=response.body();
                CarGuzhangInfo.ResultDTO resultDTO = qi.getResult();
                resultDTO.save();//存储数据
                Log.d(TAG, "onResponse: "+resultDTO.getZwhy());
            }

            @Override
            public void onFailure(Call<CarGuzhangInfo> call, Throwable t) {

                Log.d(TAG, "onFailure: "+t);
            }
        });
    }

    private void getCarInfo() {
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl("http://www.hnzzsgz.cn:5001/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        CarlistService carlistService = retrofit.create(CarlistService.class);

        Call<CarInfo> all=carlistService.getAll();

        all.enqueue(new Callback<CarInfo>() {
            @Override
            public void onResponse(Call<CarInfo> call, Response<CarInfo> response) {
                CarInfo ca = response.body();
                String s=ca.getData().get(0).getCarplate();
                textView.setText(s);

            }

            @Override
            public void onFailure(Call<CarInfo> call, Throwable t) {

            }
        });
    }
}