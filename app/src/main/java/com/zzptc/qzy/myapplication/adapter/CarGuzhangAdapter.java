package com.zzptc.qzy.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.zzptc.qzy.myapplication.R;
import com.zzptc.qzy.myapplication.entity.CarGuzhangInfo;

import java.util.List;

public class CarGuzhangAdapter extends ArrayAdapter<CarGuzhangInfo> {
    int resourceid;
    List<CarGuzhangInfo> list;

    public CarGuzhangAdapter(@NonNull Context context, int resource, @NonNull List<CarGuzhangInfo> objects) {
        super(context, resource, objects);
        this.resourceid=resource;
        this.list=objects;
    }

    @Override
    public View  getView(int position, View convertView, ViewGroup parent){
        CarGuzhangInfo resultDTO=getItem(position);   //获取当前项目CarGuzhangInfo实例化
        if (convertView==null){
            convertView= LayoutInflater.from(getContext()).inflate(resourceid,null);
        }
        TextView tv_cade=convertView.findViewById(R.id.tv_code);
        TextView tv_sycx=convertView.findViewById(R.id.tv_sycx);
        TextView tv_zwhy=convertView.findViewById(R.id.tv_zwhy);
        TextView tv_ywhy=convertView.findViewById(R.id.tv_ywhy);
        TextView tv_gzfw=convertView.findViewById(R.id.tv_gzfw);
        TextView tv_ms=convertView.findViewById(R.id.tv_ms);

        tv_cade.setText(resultDTO.getResult().getCode());
        tv_sycx.setText(resultDTO.getResult().getSycx());
        tv_zwhy.setText(resultDTO.getResult().getZwhy());
        tv_ywhy.setText(resultDTO.getResult().getYwhy());
        tv_gzfw.setText(resultDTO.getResult().getGzfw());
        tv_ms.setText(resultDTO.getResult().getMs());

        return  convertView;
    }
}
